<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>格式化文本</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="">
	<meta http-equiv="description" content="">
	<link rel="stylesheet" href="<%=basePath%>js/colorpicker/css/jquery.bigcolorpicker.css" type="text/css" />
	<script type="text/javascript" src="http://www.jq-school.com/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/colorpicker/js/jquery.bigcolorpicker.js"></script>
	<script type="text/javascript">
	
	</script>
	
</head>
<body>
	<div style="margin: 10px auto;border: solid 1px #F0F0F0;padding: 10px;" >
	
		<div id="title">
			${txt }
		</div>
		<div>
			<input id="selectColor" type="button" value="选色" />
			<select id="font-size-select">
			</select>
			<input type="checkbox" id="strong_check"/>加粗
			<input type="checkbox" id="em_check"/>斜体
			<input type="checkbox" id="u_check"/>下划线
		</div>
	</div>
</body>
  <script type="text/javascript"> 
    
    var curColor="";
    var strong="";
    var em="";
    var u="";
    var size=""; 
    function submit(){
    	return {curColor:curColor,strong:strong,em:em,u:u,size:size};
    }
    $(document).ready(function(){ 
 		$("#selectColor").bigColorpicker(function(el,color){
				$("#title").css("color",color);  
				curColor=color;
			}); 
    	initFontSize(); 
    	$("#font-size-select").change(function(){changeFontSize()});
    	$("input[type='checkbox']").change(function(e){checkbox(e)});
    });
    function checkbox(e){
    	var val=$(e.target)[0].checked;
    	var t=e.target.id; 
    	if(t.indexOf("strong")!=-1){
    	if(val){
    		$("#title").css("font-weight","bold");
    		strong="1";
    	}else{
    		$("#title").css("font-weight","normal");
    		strong="0";
    	}
    	}else if(t.indexOf("em")!=-1){ 
    	if(val){
	    	$("#title").css("font-style","italic");
	    	em="1";
	    }else{
	    	$("#title").css("font-style","normal");
	    	em="0";
	    }
    	}else if(t.indexOf("u_")!=-1){
    	if(val){
	    	$("#title").css("text-decoration","underline");
	    	u="1";
	    }else{
	    	$("#title").css("text-decoration","");
	    	u="0";
	    }
    	}
    }
    function changeFontSize(){ 
   // alert('ddd');
    	//alert($("#font-size-select").find("option:selected").val());
    	$("#title").css("font-size",$("#font-size-select").find("option:selected").val()+"px");
    	size=$("#font-size-select").find("option:selected").val()+"px";
    	//$("#title font").attr("size",$("#font-size-select").find("option:selected").val()+"px");
    }
   function initFontSize(){
   	for(i=4;i<=30;i++){
	   	if(i==30){
	   		var s="<option value='"+i+"'>"+i+"</option>";
	   		$("#font-size-select").append(s);
	   	}else{
	   		var s="<option value='"+i+"' selected>"+i+"</option>";
	   		$("#font-size-select").append(s);
	   	}
   	}
   	
   }
    </script>
</html>
