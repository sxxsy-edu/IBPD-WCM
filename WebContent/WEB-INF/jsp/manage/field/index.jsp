<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>内容模型-字段管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
		<div region="west" border="true">


            <table id="grid" style="width: 900px" title="字段列表" iconcls="icon-view">            
            </table>


		</div>
	</div>
	<div id="mm" class="easyui-menu" style="width:120px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
	</div>
	<div id="addDialog" class="easyui-window"  title="" collapsible="false" minimizable="false"
        maximizable="false" icon="icon-save"  style="width: 800px; height: 400px; padding: 5px;
        background: #fafafa;">
		<jsp:include page="/manage/field/add.jsp"></jsp:include>
	</div>
	
	<script type="text/javascript">
	//实现对DataGird控件的绑定操作
        function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>manage/field/list.do?modelId=${param.modelId}',   //指向后台的Action来获取当前菜单的信息的Json格式的数据
                title: '内容模型-字段列表',
                iconCls: 'icon-grid',
                singleSelect:false,
                height: 650,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: true,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
				    $('#mm').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				
                columns: [[
                    { field: 'ck', checkbox: true },   //选择
                     { title: '字段显示名称', field: 'displayName', width: 150, sortable:true },
                     { title: '表单名称', field: 'formName', width: 150, sortable:true },
                     { title: '字段类型', field: 'htmlType', width: 100, sortable:true },
                     { title: '验证模型', field: 'validateModelId', width: 100, sortable:true },
                     { title: '排序', field: 'order', width: 100, sortable:true },
                     { title: '默认值', field: 'defaultValue', width: 100, sortable:true },
                     { title: '可选值', field: 'optionValue', width: 100, sortable:true },
                     { title: '字段类别', field: 'fieldType', width: 100, sortable:true },
                     { title: 'ID', field: 'id', sortable:true },
                  	 { title: '内容模型Id', field: 'modelId', sortable:true,hidden:true },
                  	 { title: '字段长度', field: 'fieldLength', width: 100, sortable:true },
                     { title: '说明', field: 'intro', width: 80, sortable:true,hidden:true }
               ]], 
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditOrViewDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '初始默认字段', 
                    iconCls: 'icon-reset',
                    handler: function () {
                        initDefaultField();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    ShowEditOrViewDialog();
                }
            })
        };
        function initDefaultField(){
        	location.href="<%=basePath %>manage/field/initDefaultField.do?modelId=${param.modelId}";
        };
        function reload(){
        	$("#grid").datagrid("reload");
        };
        function ShowAddDialog(){
        	setFieldValue('','${param.modelId}','','','','','','','','','','');
        	$('#addDialog').window('open');
        };
        function setFieldValue(id,modelId,displayName,formName,htmlType,validateModelId,order,defaultValue,optionValue,fieldType,intro,fieldLength){
        
        		$("#id").val(id);
        		$("#modelId").val(modelId);
				$("#displayName").val(displayName);
				$("#formName").val(formName);
				$("#htmlType").val(htmlType); 
				$("#validateModelId").val(validateModelId); 
				$("#order").val(order); 
				$("#defaultValue").val(defaultValue); 
				$("#optionValue").val(optionValue); 
				$("#fieldType").val(fieldType); 
				$("#intro").val(intro); 
				$("#fieldLength").val(fieldLength); 
        
        };
        function ShowEditOrViewDialog(){
        	 var row = $('#grid').datagrid('getSelected');
			if (row){	
			$("#fm").attr("action","<%=basePath%>manage/field/doEdit.do");
				setFieldValue(row.id,row.modelId,row.displayName,row.formName,row.htmlType,row.validateModelId,row.order,row.defaultValue,row.optionValue,row.fieldType,row.intro,row.fieldLength);
				$('#addDialog').window('open');
			}else{
				$.messager.alert('提示','请选择记录后再操作');
			}
        };
        function del(){
	        var row = $('#grid').datagrid('getSelections');
	        if(row.length>0){
	        	$.messager.confirm('提示','确定删除?',function(r){
		        	if(r){
			        	var ids="";
			        	$.each(row, function (index, item) {
		            		ids += item.id + ",";
		        		});
		        		ids=ids.substring(0,ids.length-1);
			        	location.href="<%=basePath %>manage/field/doDel.do?ids="+ids+"&modelId=${param.modelId}";
		        	}
	        	});
	        }else{
	        	$.messager.alert('提示','没有选中记录');
	        }
        };
        $(function(){
        	InitGrid("");//中间设置查询参数，该参数需要从request的流中读取
        	$('#addDialog').window({
                title: '添加新的字段',
                width: 800,
                modal: true,
                shadow: true,
                closed: true,
                height: 400,
                resizable:false
            });
        	$('#addDialog').window('close');
        });
	</script>
	</body>
</html>
