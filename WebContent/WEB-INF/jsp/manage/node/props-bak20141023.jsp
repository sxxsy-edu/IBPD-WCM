<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:80px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr td input {
			width:130px;
		}
		.baseInfo tr td select {
			width:130px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   <div region="east" hide="true" split="false"  style="width:230px;" id="west">
		<div id="propPanel" class="easyui-accordion" fit="true" border="false">
		<input type="hidden" id="nodeId" value="${node.id }"/>
			<div title="基本信息">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<td class="tit">栏目名称</td>
						<td>
							<input type="text" mthod="smt" name="text" value="${node.text }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目关键字</td>
						<td>
							<input type="text" mthod="smt" name="keywords" value="${node.keywords }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目类型</th>
						<td>
							<select mthod="smt" name="nodeType">
								<option id="0" <c:if test="${node.nodeType=='0' }">selected</c:if>>本地栏目</option>
								<option id="1" <c:if test="${node.nodeType=='1' }">selected</c:if>>转向链接</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">转向地址</th>
						<td>
							<input type="text" mthod="smt" name="linkUrl" value="${node.linkUrl }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">所属分组</th>
						<td>
							<input type="text" mthod="smt" name="group" value="${node.group }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">统计单位</th>
						<td>
							<input type="text" mthod="smt" name="subContentCountUnit" value="${node.subContentCountUnit }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目状态</th>
						<td>
							<select mthod="smt" name="state">
								<option id="1" <c:if test="${node.state==1 }">selected</c:if>>正常开通</option>
								<option id="-1" <c:if test="${node.state==-1 }">selected</c:if>>锁定栏目</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">全局排序</th>
						<td>
							<input type="text" mthod="smt" name="order" value="${node.order }"/>
						</td>
					</tr>
					<tr><td class="tit">栏目表单模型</th>
						<td>
							<select mthod="smt" name="nodeMediaId">
								<option id="1" <c:if test="${node.nodeMediaId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
						</td>
					</tr>
					<tr><td class="tit">内容表单模型</th>
						<td>
							<select mthod="smt" name="contentMediaId">
								<option id="1" <c:if test="${node.contentMediaId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目管理员</th>
						<td>
							<input type="text" mthod="smt" name="manageUser" value="${node.manageUser }"/>
						</td>
					</tr>
					<tr>
					<tr>
						<td class="tit">栏目说明</td>
						<td>
							<input type="text" mthod="smt" name="description" value="${node.description }"/>
						</td>
					</tr>
				</table>
			</div>
				
			<div title="展现设置">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<td class="tit">导航中显示</th>
						<td>
							<select mthod="smt" name="displayInNavigator">
								<option id="true" <c:if test="${node.displayInNavigator==true }">selected</c:if>>显示</option>
								<option id="false" <c:if test="${node.displayInNavigator==false }">selected</c:if>>隐藏</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">导航排序</th>
						<td>
							<input type="text" mthod="smt" name="orderByNavigator" value="${node.orderByNavigator }"/>
						</td>
					</tr>
						<td class="tit">模板库ID</th>
						<td>
							<select mthod="smt" name="templateGroupId">
								<option id="-1" <c:if test="${node.templateGroupId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">背景音乐</th>
						<td>
							<input type="text" class="easyui-filebox" mthod="smt" name="bgMusic" value="${node.bgMusic }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目logo</th>
						<td>
							<input type="text" mthod="smt" name="logo" value="${node.logo }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">栏目图标</th>
						<td>
							<input type="text" class="easyui-filebox" mthod="smt" name="icon" value="${node.icon }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">标志图</th>
						<td>
							<input type="text" class="easyui-filebox" mthod="smt" name="map" value="${node.map }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">banner图片</th>
						<td>
							<input type="text" class="easyui-filebox" mthod="smt" name="banner" value="${node.banner }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">版权信息</th>
						<td>
							<input type="text" mthod="smt" name="copyright" value="${node.copyright }"/>
						</td>
					</tr>
				</table>			
			</div>
	    </div>
    </div>
	</div>
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		$("[mthod='smt']").bind("blur",function(e){
			var nodeId=$("#nodeId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("id");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/Node/saveProp.do",
				{id:nodeId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
	});        
     </script>
     
	</body>
</html>
