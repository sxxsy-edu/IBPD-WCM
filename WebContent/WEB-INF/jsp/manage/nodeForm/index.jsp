<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle}</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/custom.js"></script>
	</head>

	<body>

		<div class="easyui-panel" fit="true" border="true" style="padding:0">
            <table id="grid" style="width: 900px;margin:0" title="字段列表" iconcls="icon-view">            
            </table>
		</div>
	
	</body>
	<script type="text/javascript">
	var path="<%=path %>";
	var basePath="<%=path %>";
	//实现对DataGird控件的绑定操作
        function InitGrid(queryData) {
            $('#grid').datagrid({   
                url: path+'/Manage/NodeForm/getFieldList.do?nodeId=${nodeId}&type=${type}',  
                title: '字段列表',
                iconCls: 'icon-grid',
                singleSelect:false,
                //height: 650,
				fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: false,
                //pageSize: 10,
                //pageList: [10,20,50,100],
                rownumbers: true,
                sortName: 'order',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
				    $('#mm').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				
                columns: [[
                     { field: 'ck', checkbox: true },
                     { title: 'ID', field: 'id', sortable:true },
                     { title: '字段名称', field: 'fieldName', width: 150, sortable:true },
                     { title: '显示名称', field: 'displayName', width: 150, sortable:true },
                     { title: '表单名称', field: 'formName', width: 150, sortable:true },
                     { title: '字段类型', field: 'htmlType', width: 100, sortable:true },
                     { title: '验证模型', field: 'validateModelId', width: 100, sortable:true },
                     { title: '排序', field: 'order', width: 100, sortable:true },
                     { title: '默认值', field: 'defaultValue', width: 100, sortable:true },
                     { title: '可选值', field: 'optionValue', width: 100, sortable:true },
                     { title: '字段类别', field: 'fieldType', width: 100, sortable:true },
                   	 { title: '内容模型Id', field: 'modelId', sortable:true,hidden:true },
                  	 { title: '字段长度', field: 'fieldLength', width: 100, sortable:true },
                     { title: '说明', field: 'intro', width: 80, sortable:true,hidden:true }
               ]], 
                toolbar: [{
                    id: 'btnBind',
                    text: '绑定新表单',
                    iconCls: 'icon-add',
                    handler: function () {
                        openAddFormDialog();
                    }
                }, '-', {
                    id: '',
                    text: '添加状态下只读/可写',
                    iconCls: 'icon-remove',
                    handler: function () {
                       readOnlyByAddMode();
                    }
                },{
                    id: '',
                    text: '添加状态下可见/隐藏',
                    iconCls: 'icon-remove',
                    handler: function () {
                       dispalyByAddMode();
                    }
                },{
                    id: '',
                    text: '编辑状态下只读/可写',
                    iconCls: 'icon-remove',
                    handler: function () {
                       readOnlyByEditMode();
                    }
                },{
                    id: '',
                    text: '编辑状态下可见/隐藏',
                    iconCls: 'icon-remove',
                    handler: function () {
                       dispalyByEditMode();
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
       function dispalyByAddMode(){
		var _ids=getSelections("grid");
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			$.post(
			basePath+"/Manage/NodeForm/hiddenByAddMode.do",
			{ids:_ids},
			function(result){
				if(result.indexOf("msg")!=-1){
					var _o=eval("("+result+")");
					if(_o.status=='99'){
						msgShow("提示",_o.msg,"warning");
					}else{
						msgShow("提示",_o.msg,"error");
					}
				}else{
					msgShow("提示","操作失败","warning");
				}
				
				reload();
			});
		}		
	   };
	   function dispalyByEditMode(){
		var _ids=getSelections("grid");
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			$.post(
			basePath+"/Manage/NodeForm/hiddenByEditMode.do",
			{ids:_ids},
			function(result){
				if(result.indexOf("msg")!=-1){
					var _o=eval("("+result+")");
					if(_o.status=='99'){
						msgShow("提示",_o.msg,"warning");
					}else{
						msgShow("提示",_o.msg,"error");
					}
				}else{
					msgShow("提示","操作失败","warning");
				}
				
				reload();
			});
		}		
	   };
	   function readOnlyByAddMode(){
		var _ids=getSelections("grid");
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			$.post(
			basePath+"/Manage/NodeForm/readOnlyByAddMode.do",
			{ids:_ids},
			function(result){
				if(result.indexOf("msg")!=-1){
					var _o=eval("("+result+")");
					if(_o.status=='99'){
						msgShow("提示",_o.msg,"warning");
					}else{
						msgShow("提示",_o.msg,"error");
					}
				}else{
					msgShow("提示","操作失败","warning");
				}
				
				reload();
			});
		}		
	   };
	   function readOnlyByEditMode(){
		var _ids=getSelections("grid");
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			$.post(
			basePath+"/Manage/NodeForm/readOnlyByEditMode.do",
			{ids:_ids},
			function(result){
				if(result.indexOf("msg")!=-1){
					var _o=eval("("+result+")");
					if(_o.status=='99'){
						msgShow("提示",_o.msg,"warning");
					}else{
						msgShow("提示",_o.msg,"error");
					}
				}else{
					msgShow("提示","操作失败","warning");
				}
				
				reload();
			});
		}		
	   };
        function reload(){
			$('#grid').datagrid("unselectAll");
			$('#grid').datagrid("reload");
        };
        
		function checkStatus(){
			$.post(
				path+'/Manage/NodeForm/check.do?nodeId=${nodeId}&type=${type}',
				{},
				function(result){
					var r=eval("("+result+")");
					if(r.status=="-1"){
						$.messager.confirm("提示",r.msg+"是否为该栏目新建表单?",function(r){
							if(r){
								openAddFormDialog();
							}
						});
						
					}else{
						InitGrid("");
					}
				}
			); 
		};
		function openAddFormDialog(){
			var addDialog=$("<div id='addFormDialog'></div>").appendTo("body");
			addDialog.dialog({
				modal:true,
				title:'新建表单',
				shadow:true,
				iconCls:'icon-edit',
				width:500,
				height:200,
				resizable:true,
				content:'<iframe src="'+path+'/Manage/NodeForm/toAdd.do?nodeId=${nodeId}&type=${type}" frameborder="0" width="100%" height="100%"></iframe>'
			});
			addDialog.dialog("open");
		};
		function reInit(){
			checkStatus();
			$("#addFormDialog").dialog("close");
			$("#addFormDialog").remove();
		};
        $(function(){
			setTimeout(function(){checkStatus();},2000);
        });
		function loadProps(id){
        parent.$("#propsPanel",parent.document).attr("src",path+"/Manage/NodeForm/props.do?fieldId="+id+"&type=${type}&t="+new Date());
        };

	</script>
</html>
