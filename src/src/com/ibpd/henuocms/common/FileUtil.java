package com.ibpd.henuocms.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
/**
 * 获取各类目录的辅助类
 * 这个类几乎已经废弃,但仍然有地方在调用
 * @author mg by qq:349070443
 *
 */
public class FileUtil {
	public static void main(String[] args){
		IbpdLogger.getLogger(FileUtil.class).info(FileUtil.getWebRootDirPath());
	}
	public static String getClassRealPath(){
		URL s=IbpdCommon.class.getResource("/");
		return s.getPath();
	}
	public static String getWebRootDirPath(){
		String  s=getClassRealPath();
		File f=new File(s); 
//		if(!f.isDirectory())
//			return "";
		File wf=f.getParentFile().getParentFile();
		return wf.getAbsolutePath();
	}
	public static void makeDir(String dirPath){
		File fl=new File(dirPath);
		if(fl.exists())
			return;
		if(fl.getParentFile().exists()){
			fl.mkdir();
		}else{
			makeDir(fl.getParentFile().getAbsolutePath());
			fl.mkdir();
		}
	}
}
