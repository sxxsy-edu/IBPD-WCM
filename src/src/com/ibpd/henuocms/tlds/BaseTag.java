package com.ibpd.henuocms.tlds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.jsp.tagext.BodyTagSupport;
/**
 * tag基类 
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:42:11
 */
public abstract class BaseTag extends BodyTagSupport {
	/**
	 * 继承bodyTagSupport后新增虚方法，用来完成初始化的工作
	 */
	protected abstract void init();
	public static boolean isNumeric(String str){ 
	    Pattern pattern = Pattern.compile("[0-9]*"); 
	    return pattern.matcher(str).matches();    
	 } 
	protected String errorString="";
	protected Iterator objList=null;
	protected Object obj=null;
	protected Object getNextObj(){
		if(objList==null)
			return null;
		if(objList.hasNext()){
//			obj=objList.next();
			return objList.next();
		}else{
			objList=null;
			return null;
		}
	}
}
