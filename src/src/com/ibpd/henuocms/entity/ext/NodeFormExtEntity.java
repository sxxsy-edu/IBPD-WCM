package com.ibpd.henuocms.entity.ext;

import java.util.List;

import com.ibpd.henuocms.entity.NodeFormEntity;
import com.ibpd.henuocms.entity.NodeFormFieldEntity;
/**
 * 扩展的栏目表单实体类，增加了表单字段的list，方便调用。
 * @author mg
 *
 */
public class NodeFormExtEntity extends NodeFormEntity {

	private List<NodeFormFieldEntity> nodeFormFieldList=null;
	public NodeFormExtEntity(NodeFormEntity nodeForm,List<NodeFormFieldEntity> nodeFormFieldList){
		this.setCreateTime(nodeForm.getCreateTime());
		this.setFormName(nodeForm.getFormName());
		this.setFormType(nodeForm.getFormType());
		this.setId(nodeForm.getId());
		this.setIsDefault(nodeForm.getIsDefault());
		this.setOrder(nodeForm.getOrder());
		this.setUpdateTime(nodeForm.getUpdateTime());
		this.setNodeFormFieldList(nodeFormFieldList);
	}
	public void setNodeFormFieldList(List<NodeFormFieldEntity> nodeFormFieldList) {
		this.nodeFormFieldList = nodeFormFieldList;
	}
	public List<NodeFormFieldEntity> getNodeFormFieldList() {
		return nodeFormFieldList;
	}
}
