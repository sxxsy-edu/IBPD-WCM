package com.ibpd.shopping.service.express;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.ExpressEntity;
@Service("expressService")
public class ExpressServiceImpl extends BaseServiceImpl<ExpressEntity> implements IExpressService {
	public ExpressServiceImpl(){
		super();
		this.tableName="ExpressEntity";
		this.currentClass=ExpressEntity.class;
		this.initOK();
	}

	public ExpressEntity getEntityByCode(String code) {
		List<ExpressEntity> el=this.getList("from "+this.getTableName()+" where code='"+code+"'",null);
		if(el==null || el.size()==0)
			return null;
		return el.get(0);
	}
}
