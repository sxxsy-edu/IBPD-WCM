package com.ibpd.shopping.service.orderlog;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.OrderLogEntity;
@Service("orderLogService")
public class OrderLogServiceImpl extends BaseServiceImpl<OrderLogEntity> implements IOrderLogService {
	public OrderLogServiceImpl(){
		super();
		this.tableName="OrderLogEntity";
		this.currentClass=OrderLogEntity.class;
		this.initOK();
	}
}
