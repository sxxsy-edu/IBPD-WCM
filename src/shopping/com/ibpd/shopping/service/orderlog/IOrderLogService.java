package com.ibpd.shopping.service.orderlog;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderLogEntity;

public interface IOrderLogService extends IBaseService<OrderLogEntity> {
}
