package com.ibpd.shopping.service.ordertmp;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderTmpEntity;

public interface IOrderTmpService extends IBaseService<OrderTmpEntity> {
	List<OrderTmpEntity> getOrderTmpList(String mOrderId);
}
