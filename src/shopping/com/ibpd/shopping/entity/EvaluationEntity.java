
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_Evaluation")
public class EvaluationEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_accId",nullable=true)
	private Long accId;
	@Column(name="f_productId",nullable=true) 
	private Long productId;
	@Column(name="f_orderId",nullable=true) 
	private Long orderId;
	@Column(name="f_accName",length=45,nullable=true)
	private String accName;
	@Lob
	@Basic(fetch=FetchType.LAZY)
	@Column(name="f_content",nullable=true)
	private String content="";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	@Column(name="f_level",nullable=true)
	private Integer level;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAccId() {
		return accId;
	}
	public void setAccId(Long accId) {
		this.accId = accId;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Long getOrderId() {
		return orderId;
	}
	
	
}
