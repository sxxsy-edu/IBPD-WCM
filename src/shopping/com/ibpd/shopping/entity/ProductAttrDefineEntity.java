
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_productAttrDefine")
public class ProductAttrDefineEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_catalogId",nullable=true)
	private Long catalogId;
	
	@Column(name="f_attrName",length=45,nullable=true)
	private String attrName="";
	
	@Column(name="f_type",length=45,nullable=true)
	private String type="";
	
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	
	@Column(name="f_defClass",nullable=true)
	private Integer defClass=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}
	public void setDefClass(Integer defClass) {
		this.defClass = defClass;
	}
	public Integer getDefClass() {
		return defClass;
	}
}
