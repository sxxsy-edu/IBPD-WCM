
package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_catalog")
public class CatalogEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_name",length=45,nullable=true)
	private String name;
	@Column(name="f_code",length=100,nullable=true)
	private String code;
	
	@Column(name="f_pid",nullable=true)
	private Long pid;
	@Column(name="f_leaf",nullable=true)
	private Boolean leaf;
	@Column(name="f_order",nullable=true)
	private Integer order;
	@Column(name="f_type",length=15,nullable=true)
	private String type;
	
	@Column(name="f_showInNav",length=2,nullable=true)
	private String showInNav="n";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order1) {
		this.order = order1;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getShowInNav() {
		return showInNav;
	}

	public void setShowInNav(String showInNav) {
		this.showInNav = showInNav;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public Boolean getLeaf() {
		return leaf;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
}
