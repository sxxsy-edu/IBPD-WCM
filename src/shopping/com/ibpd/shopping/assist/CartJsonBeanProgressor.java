package com.ibpd.shopping.assist;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonBeanProcessor;

import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.CartEntity;
import com.ibpd.shopping.entity.ProductEntity;

public class CartJsonBeanProgressor implements JsonBeanProcessor {

	public JSONObject processBean(Object arg0, JsonConfig arg1) {
		if(arg0 instanceof ExtTenantCartEntity){
			return JSONObject.fromObject(arg0);
		}
	
		return null;
	} 

}
